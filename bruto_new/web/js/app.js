	(function(){
		var app = angular.module('portfolioApp', ['ngSanitize']);
		app.controller('portfolioController', function ($scope, $http){
			var dataUrl = 'data/data.json' + '?callback=JSON_CALLBACK';
			
			$http.get(dataUrl).then(function(response){
				$scope.data = response.data;
				$scope.pdata = $scope.data[0];
			});
		});

		app.controller('empController', function ($scope, $http){
			var employerDataUrl = 'data/data.json' + '?callback=JSON_CALLBACK';

			$http.get(employerDataUrl).then(function(response){
				$scope.data = response.data;
				$scope.empData = $scope.data[1];
			});
			$scope.myHTML = 
			"I am an <code>HTML</code>string with " +
			'<a href="#">links!</a> and other <em>stuff</em>';
			
		});



	})();

// (function(){
// 	var app = angular.module('portfolioApp', ['ngSanitize']);
// 		app.controller('portfolioController', [ '$scope', function($scope){
// 			this.pdata = pInfo;
// 			 $scope.myHtml = "<p>Hi, I'm Barry, I am a front end developer. I have been working with websites for many years and really enjoy it. I love writing clean efficient code and keeping up to date with front end trends.</p><p>I have more than 5 years experience working with <strong>HTML</strong> and <strong>CSS</strong> and more recently adding <strong>JQuery</strong> to my skill sets, as well as <strong>HTML5</strong>, <strong>CSS3</strong>, <strong>LESS</strong>, <strong>Bootstrap</strong>, <strong>GIT</strong>, <strong>CMV</strong> php environment (<strong>Yii</strong>)</p><p>Creating pixel perfect responsive UX layouts are what I most enjoy working on as well as investigating new techniques and frameworks such as the flexbox technique.</p>";						
// 		}]);
// 		app.controller('empController', function(){
// 			this.empdata = empInfo;
// 		});

// 	// app.controller('formCtrl'['$scope', function(){
// 	// 	$scope.myForm = {};
// 	// }]);
// 		app.controller('storageService', function ($rootScope) {
// 		    return {   
// 		        get: function (key) {
// 		           return localStorage.getItem(key);
// 		        },
// 		        save: function (key, data) {
// 		           localStorage.setItem(key, JSON.stringify(data));
// 		        },
// 		        remove: function (key) {
// 		            localStorage.removeItem(key);
// 		        },    
// 		        clearAll : function () {
// 		            localStorage.clear();
// 		        }
// 		    }
// 	    });
// 		app.controller('formController', function ($scope,storageService) {
// 			$scope.tittle = "Primer Controller";
// 			$scope.txtBtn = "Mostrar";
// 				$scope.muestra = function(){
// 					$scope.capa = $scope.capa===1? 0:1;
// 					if($scope.capa===1){
// 						$scope.txtBtn = "Ocultar";
// 					}else{
// 						$scope.txtBtn = "Mostrar";
// 					}	
// 				}
// 		 	//prepramos la variable Json
// 			$scope.model = {};
// 				$scope.submitForm = function (model) {
// 					storageService.save('datossonJ',model);
// 					//storageService.get('datossonJ');
// 					alert('Guardado en localStorage!!!');
// 				}
// 		});

// 		app.controller('otroController', function ($scope,storageService) {
// 			$scope.tittle="Segundo Controller";
// 		 		$scope.muestraStorage = function () {
// 			 		$scope.capa=$scope.capa===1? 0:1;	
// 					$scope.jitijander=storageService.get('datossonJ');
// 				},

// 				$scope.deleteStorage = function () {	
// 					$scope.jitijander=storageService.remove('datossonJ');
// 				}
// 		});
// 		var pInfo = {
// 			name: "Barry Prendergast",
// 			title: "Front-End Developer",
// 			email: "barryprendergast78@gmail.com",
// 			likedin: "http://es.linkedin.com/in/BarryPdrgst",
// 			skills: [
// 				"HTML5",
// 				"XHTML",
// 				"CSS(3)",
// 				"LESS",
// 				"JQuery",
// 				"Angular.js",
// 				"Cross browser compatibility",
// 				"Responsive Web Design",
// 				"Git",
// 				"Phohtoshop"
// 				],
// 			img: "img/me.png",
// 			education: [{
// 				title: "Teamtreehouse.com (online education site for web professionals)",
// 				duration: "Nov 2013 – Nov 2014 ",
// 				discription: [
// 					"Front-end Web Development: HTML5, CSS3",
// 					"Programming: JavaScript Foundations, JQuery Basics, Ajax Basics, Console Foundations, Accessibility,",
// 				]
// 			},
// 			{
// 				title: "Ondas Escolares. Madrid",
// 				duration: "October 2012",
// 				discription: ["Web Design and development of web applications with HTML5,CSS3 and  jQuery",
// 					"",
// 				]
// 			},
// 			{
// 				title: "Centro de enseñanza Adams. Madrid.",
// 				duration: "October-February 2008",
// 				discription: ["Web design and multimedia. Photoshop CS2, Dreamweaver 8, Flash 8, Fireworks 8, HTML and script languages"]
// 			}],
// 			interests: "When I'm not working on personal web projects or helping friends with theirs, I like to unwind playing guitar, mainly punk/rock music very loudly in a local band."
// 		};	
// 		var empInfo = {
// 			empresas: [{
// 				name: "Vivocom",
// 				date: "september 2014 - march 2015",
// 				url: "www.vivocom.eu",
// 				description: [
// 				"Front End Developer HTML5, CSS3/LESS, Jquery.",
// 				"Code layouts for web applications  and  mobile devices/Responsive Design with HTML5, CSS3/LESS y Jquery.",
// 				"Mainly on the  Keepunto.com project. Working with frameworks like Bootstrap, Yii PHP Framework. GitVCS.",
// 				],
// 				examples: [{
// 					name: "Keepunto",
// 					description: "Code layouts from designer (psd) in HTML 5, CSS 3 and jquery. As the only front end developer I had to handle all the front end issues and modificaciones. The project is built on the Yii php CMV framework as well as bootstrap css framework and managed through GitHub VCS",
// 					skills: "html5,css3,jquery",
// 					image: "img/keepunto/keepunto1.png",
// 					link: "www.keepunto.com",
// 				},
// 				{
// 					name: "Scool Zone",
// 					description: "Code layouts from designer (psd) in HTML 5, CSS 3 and jquery. As the only front end developer I had to handle all the front end issues and modificaciones. The project is built on the Yii php CMV framework as well as bootstrap css framework and managed through GitHub VCS",
// 					skills: "html5,css3,jquery",
// 					image: "img/keepunto/sCool-Zone-Keepunto.png",
// 					link: "www.keepunto.com",
// 				}]	
// 			},
// 			{
// 				name: "Vocento",
// 				date: "May 2013 – july 2014",
// 				url: "www.vocento.com",
// 				description: [
// 				"Front End Developer HTML5, CSS3/LESS, Jquery.",
// 				"Collaborating in two of the media groups’ web projects, GUAPABOX and  ABC FOTO. Part of the team of 3 front end developers working on the general redesign of the groups 11 regional news websites.",
// 				],
// 				examples: [{
// 					name: "Guapabox",
// 					description: "Code layouts from designer (psd) in HTML 5, CSS 3 and jquery. As the only front end developer I had to handle all the front end issues and modificaciones. The project is built on the bootstrap css framework",
// 					skills: "html5,css3,jquery",
// 					image: "img/vocento/Guapabox1.png",
// 					link: "www.guapabox.com",
// 				},
// 				{
// 					name: "Redesign regional news sites",
// 					description: "Code layouts from designer (psd) in HTML 5, CSS 3 and jquery. As the only front end developer I had to handle all the front end issues and modificaciones. The project is built on the bootstrap css framework",
// 					skills: "html5,css3,jquery",
// 					image: "img/vocento/Redesign1.png",
// 					link: "www.elcorreo.com",
// 				},
// 				{
// 					name: "ABCFoto",
// 					description: "Code layouts from designer (psd) in HTML 5, CSS 3 and jquery. As the only front end developer I had to handle all the front end issues and modificaciones. The project is built on the bootstrap css framework",
// 					skills: "html5,css3,jquery",
// 					image: "img/vocento/abcfoto1.png",
// 					link: "abcfoto.abc.es",	
// 				}]	
// 			},
// 			{
// 				name: "Grupo Mercantis",
// 				date: "May 2008 – November 2011",
// 				url: "www.grupomercantis.com",
// 				description: [
// 				"HTML, CSS, Wordpress, Oscommerce, phpbb. Web editing and maintenance work in a team of web developers and programmers.",
// 				"Localization Work: Adapting websites en 12 languages  editing html/css, flash  (texts, images, animations) flash banners, image editing.",
// 				],
// 			}]	
// 		};
// 	// angular.module('contForm', [])
// 	// 	.controller('formCtrl', ['$scope', function($scope) {
// 	// 	  $scope.master = {};
// 	// 	  $scope.update = function(user) {
// 	// 	    $scope.master = angular.copy(user);
// 	// 	  };
// 	// 	  $scope.reset = function(form) {
// 	// 	    if (form) {
// 	// 	      form.$setPristine();
// 	// 	      form.$setUntouched();
// 	// 	    }
// 	// 	    $scope.user = angular.copy($scope.master);
// 	// 	  };
// 	// 	  $scope.reset();
// 	// }]);

// })();


















