var busStopApp = angular.module('busStopApp', ['google-maps']);
busStopApp.controller('busStopCtrl', function ($scope, $http) {
	stopsUrl = 'https://api.tfl.gov.uk/line/24/stoppoints/',
	arrivalsUrl = 'https://api.tfl.gov.uk/StopPoint/' + '490000252S' + '/arrivals',
  	$http({
		method: 'GET', 
		url: stopsUrl,
		}).success(function(data, status, headers, config) {
		      $scope.stopData = data; 
                angular.forEach($scope.stopData, function(stops){
                    stops.stopsLat = stops.lat;
                    stops.stopsLon = stops.lon;
                    //console.log(stops.stopsLat, stops.stopsLon);
                });
		}).error(function(data, status, headers, config) {
		      alert("Ha fallado la petición. Estado HTTP:"+ status);
		});
        $scope.map = {
            center: {
                latitude: 51.5105062, 
                longitude: -0.1380991
            }, 
            zoom: 13,
            options : {
                scrollwheel: false
            },
            control: {}
        }   
        $scope.marker = {
            id: 0,
            coords: {
                latitude: 51.5105062, 
                longitude: -0.1380991
            },
            options: {
                draggable: false
            }
        };

       
 	$scope.search=function(){
 		var id = $scope.stop;
        $scope.stopId = $scope.stopData[id].id;
        $scope.stopName = $scope.stopData[id].commonName;
        $scope.stopLat = $scope.stopData[id].lat;
        $scope.stopLon = $scope.stopData[id].lon;
        $scope.stopLines = $scope.stopData[id].lines;
        $scope.arrivalId = $scope.stopData[id].id;
        $scope.marker = [];
        $scope.map = {
            center: {
                latitude: $scope.stopLat, 
                longitude:  $scope.stopLon
            }, 
            zoom: 19,
            options : {
                scrollwheel: false
            },
            control: {}
        };    
        $scope.marker = {
            id: 0,
            coords: {
                latitude: $scope.stopLat, 
                longitude: $scope.stopLon
            },
            options: {
                draggable: true
            }
        };
        $http({
		method: 'GET', 
		url: 'https://api.tfl.gov.uk/StopPoint/' + $scope.arrivalId + '/arrivals',
		}).success(function(data, status, headers, config) {
		      $scope.arrivalData = data;
            angular.forEach($scope.arrivalData, function(arrival){
                arrival.minutes = (arrival.timeToStation / 60);
                arrival.seconds =  (arrival.timeToStation % 60);
        });
		}).error(function(data, status, headers, config) {
		      alert("Ha fallado la petición. Estado HTTP:"+ status);
		});
	}
    

});

