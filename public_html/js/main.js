
$(function(){
	$("small.showBtn").click(function(){ //show or hide examples
		if ( $(this).next().css('display') == 'none'){ 
			$('span',this).replaceWith("<span>hide examples</span>"); }
		else { $('span',this).replaceWith("<span>view examples</span>"); }
		$(this).next().toggle("200");
	});

	document.documentElement.className += 
    (("ontouchstart" in document.documentElement) ? ' touch' : ' no-touch');
});